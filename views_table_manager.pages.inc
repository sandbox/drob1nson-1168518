<?php
/**
 * @file
 * This file contains all functions relating to AJAX, and the management
 * of saved user data.
 */


function views_table_manager_save_cols_layout() {

  global $user;

  $cols = array();

  foreach ($_POST['cols'] as $key => $value) {
    $cols[] = str_replace(array('views-field views-field-', '-', ' ', 'active'), array('', '_', '', ''), $value);
  }

  $view_name = trim($_POST['view']);

  if (empty($view_name) || empty($cols)) {
    drupal_set_message(t('No view name or no columns to rearrange.'), 'error');
    return;
  }

  $exists = db_result(db_query("SELECT 1 FROM {views_table_manager_layout} WHERE uid = %d AND view='%s'", array($user->uid, $view_name)));

  if ($exists) {
    db_query("UPDATE {views_table_manager_layout} set cols='%s' WHERE view='%s' AND uid = %d", array(serialize($cols), $view_name, $user->uid));
  }
  else {
    db_query("INSERT INTO {views_table_manager_layout} (uid,cols,view) VALUES (%d, '%s', '%s')", array($user->uid, serialize($cols), $view_name));
  }
}


function views_table_manager_cols_form(&$form_state, $view_name, $display_id) {
  $view = views_get_view($view_name);
  if (!$view || !$view->set_display($display_id)) {
    return array();
  }

  $options = array();
  $view_option_fields = $view->display_handler->get_option('fields');
  foreach ($view_option_fields as $key => $value) {
    if ($value['label']) {
      $options[views_css_safe($value['id'])] = $value['label'];
    }
  }
 
  if (!empty($view_option_fields['title'])) {
    $options['title'] = t('Title');
  }


  $settings = db_fetch_object(db_query("SELECT * FROM {views_table_manager_visible} WHERE uid = %d AND view = '%s'", array($GLOBALS['user']->uid, $view_name)));

  $default_options = array();

  if ($settings->cols) {
    $default_options = unserialize($settings->cols);
  }

  $form['views_table_manager'] = array(
    '#type' => 'fieldset',
    '#title' => 'Manage Columns',
  );

  $form['views_table_manager']['col_visibility'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Choose the table columns which should be visible',
    '#options' => $options,
    '#default_value' => $default_options,
  );

  $form['views_table_manager']['view_name'] = array(
    '#type' => 'hidden',
    '#default_value' => $view_name,
  );

  $form['views_table_manager']['view_display'] = array(
    '#type' => 'hidden',
    '#default_value' => $display_id,
  );

  $form['views_table_manager']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'submit',
  );

  $form['#ajaxsubmit'] = TRUE;

  return $form;
}

function views_table_manager_cols_form_submit($form, &$form_state) {
  global $user;

  $view_name = $form_state['values']['view_name'];
  $display_id = $form_state['values']['view_display'];
  $view = views_get_view($view_name);
  if (!$view || !$view->set_display($display_id)) {
    return;
  }

  $settings = array();
  if (is_array($form_state['values']['col_visibility'])) {
    foreach ($form_state['values']['col_visibility'] as $key => $value) {
     $settings[views_css_safe($key)] = views_css_safe($value);
    }
  }
 
  $exists = db_result(db_query("SELECT 1 FROM {views_table_manager_visible} WHERE uid = %d AND view = '%s'", array($user->uid, $view_name)));
  if ($exists) {
    db_query("UPDATE {views_table_manager_visible} set cols = '%s' WHERE view = '%s' AND uid = %d", array(serialize($settings), $view_name, $user->uid));
  }
  else {
    db_query("INSERT INTO {views_table_manager_visible} (uid, cols, view) VALUES (%d,'%s','%s')", array($user->uid, serialize($settings), $view_name));
  }
}


/*
 * Return a form containing configurable column visibility
 * */

function views_table_manager_return_cols_form($view_name, $view_display) {

  $output = drupal_get_form('views_table_manager_cols_form', $view_name, $view_display);
  print $output;
}

function views_table_manager_return_filter_data($view_name, $label) {

  if ($view_name && $label) {
    global $user;
    $obj = db_fetch_object(db_query("SELECT * FROM {views_table_manager_filters} WHERE view='%s' AND uid='%d' AND label='%s'", array($view_name, $user->uid, $label)));
    $setting = unserialize($obj->filters);
    drupal_json($setting);
  } 
}

function views_table_manager_update_filter_settings($view_name = NULL, $view_label = NULL) {
  if ($view_name && $view_label) {

    $settings = array();
    
    foreach ($_POST as $key => $value) {
      if (!strstr($key, 'view') && !strstr($key, 'pager')) {
        $settings[$key] = $value;
      }
    }

    global $user;
    db_query("UPDATE {views_table_manager_filters} SET filters='%s' WHERE view = '%s' AND label = '%s' AND uid = '%d' ", array(serialize($settings), $view_name, $view_label, $user->uid));
  }
}

function views_table_manager_save_filter_settings($form_state) {

 $filter = explode("&", $form_state['values']['filter_settings']);
  $filter_settings = array();
  
  foreach ($filter as $key => $value) {
   $form_element = explode("=", $value);
   if (!strstr($form_element[0], 'view_') && !strstr($form_element[0], 'pager_')) {
     $filter_settings[$form_element[0]]=$form_element[1];
   }
  }

  global $user;

  db_query("DELETE FROM {views_table_manager_filters} WHERE uid='%d' AND view='%s' AND label='%s'", array($user->uid, $form_state['values']['view'], $form_state['values']['label']));
  
  db_query("INSERT INTO {views_table_manager_filters} (uid, filters, view, label) VALUES ('%d','%s','%s','%s')",
    array($user->uid, serialize($filter_settings), $form_state['values']['view'], $form_state['values']['label']));

}


function views_table_manager_return_label_form() {
  $output = drupal_get_form('views_table_manager_label_form');
  print $output;
}

function views_table_manager_update_label_settings($view_name = NULL, $type = NULL, $view_label = NULL) {
  if ($view_name && $view_label) {

    //update the view filter values
    views_table_manager_update_filter_settings($view_name, $view_label);

    //get all the view settings 
    $settings = view_table_manager_get_settings($view_name);
    global $user;

    db_query("UPDATE {views_table_manager_labels} SET setting='%s' WHERE view = '%s' AND uid = '%d' AND labelname='%s' and labeltype='%s'", array(serialize($settings), $view_name, $user->uid, $view_label, $type));

  }
}

function views_table_manager_delete_label_settings($view_name = NULL, $view_label = NULL, $type = NULL) {
  if ($view_name && $view_label) {
    global $user;
    db_query("DELETE FROM {views_table_manager_labels} WHERE view = '%s' AND uid = '%d' AND labelname='%s' AND labeltype='%s' ", array($view_name, $user->uid, $type, $view_label));
  }
}

function views_table_manager_return_labels($view_name = NULL, $labeltype = NULL) {
  global $user;
  $result = db_query("SELECT * FROM {views_table_manager_labels} WHERE view='%s' AND uid='%d' AND labeltype='%s'", array($view_name, $user->uid, $labeltype));

  $output = "";
  while ($obj = db_fetch_object($result)) {
    $output .= "<div class='label-row-wrapper'><div class='arrow'><span class='arrow-span'>▼</span></div><div label='" . $obj->labelname . "' class='label-row'>" . str_replace("-", " ", $obj->labelname) . "</div><br/></div>";
  }

  print $output;
}

function views_table_manager_return_labeldata($view_name = NULL, $label = NULL, $type = NULL) {
  if ($view_name && $label) {
    global $user;
     
    $obj = db_fetch_object(db_query("SELECT * FROM {views_table_manager_labels} WHERE view='%s' AND labelname='%s' AND uid='%d' AND labeltype='%s'", array($view_name, $label, $user->uid, $type)));
    
    //make the css references safe
    $setting = unserialize($obj->setting);
    $setting['col_visibility'] = views_table_manager_css_safe_array($setting['col_visibility']);

    drupal_json($setting);
  }
}

function views_table_manager_label_form() {
  $form['views_table_manager']['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('Enter a label to identify the table settings'),
    '#attributes' => array('class' => 'label-input')
  );

  $form['views_table_manager']['view'] = array(
    '#type' => 'hidden',
    '#default_value' => arg(2),
  );
  
  $form['views_table_manager']['labeltype'] = array(
    '#type' => 'hidden',
    '#default_value' => arg(3),
  );  

  $form['views_table_manager']['filter_settings'] = array(
    '#type' => 'hidden'
  );    
  
  $form['views_table_manager']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'submit',
  );

  $form['#ajaxsubmit'] = TRUE;

  return $form;
}

function views_table_manager_label_form_submit($form, &$form_state) {
  global $user;
  
  $settings = array();
  
  //save the current filter settings
  if ($form_state['values']['labeltype'] == "filters") {
    views_table_manager_save_filter_settings($form_state);
  }

  //get the current layout / visibility / filters and save this

  $settings = view_table_manager_get_settings($form_state['values']['view']);

  
  db_query("INSERT INTO {views_table_manager_labels} (uid, setting, view, labelname, labeltype) VALUES ('%d', '%s', '%s', '%s', '%s')",
    array($user->uid, serialize($settings), $form_state['values']['view'], str_replace(" ", "-", $form_state['values']['label']), $form_state['values']['labeltype']));
  
}

function views_table_manager_label_form_validate($elements, &$form_state, $form_id = NULL) {
  global $user;
  $obj = db_result(db_query("SELECT 1 FROM {views_table_manager_labels} WHERE labelname = '%s' and view='%s' and uid='%s' and labeltype='%s'", array($form_state['values']['label'], $form_state['values']['view'], $user->uid, $form_state['values']['labeltype'])));
  if ($obj) {
    form_set_error('edit-label', 'The label already exists');
  }
}

