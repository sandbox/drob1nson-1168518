<?php

/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */

if(!empty($settings_available)) :?>
<div class='views-table-manager-settings'>
<?php 
endif
?>

<?php if (!empty($save_filter_settings)) : ?>
<div class="save-settings-wrapper" id="filters">
  <div class="labels-wrapper">
    <div class="table-settings-labels-header">Manage saved filters</div>
    <div class="custom-labels"></div>
    <div class="create-new-label">
      <div title=" " class="link" >Create new label</div>
     </div>
  </div>
  <img src="<?php print $tablemanager_module_path; ?>/img/save-filter.png" class="save-table-settings" title="Save filter" alt="Save filter" />
</div>
<?php endif; ?>

<?php if (!empty($manage_col_widths)) : ?>
  <img src="<?php print $tablemanager_module_path; ?>/img/resize.png" class="resize-icon" title="Click here to resize table columns" alt="Click here to resize table columns" />
<?php endif; ?>

<?php if (!empty($manage_columns)) : ?>
  <img  src="<?php print $tablemanager_module_path; ?>/img/emblem-system.png" class="manage-cols-emblem" title="Click here to configure which columns are visible" alt="Click here to configure which columns are visible" />
<?php endif; ?>

<?php if (!empty($save_table_settings)) : ?>
  <div class="save-settings-wrapper" id="profile">
   
   <div class="save-table-img-wrapper">
    <img  src="<?php print $tablemanager_module_path; ?>/img/table.png" id="profile" class="save-table-settings" title="Click here to save column settings" alt="Click here to save column settings" />
   </div>  
   <div class="labels-wrapper">
    <div class="table-settings-labels-header">Manage saved table layouts</div>
    <div class="custom-labels"></div>
    <div class="create-new-label">
     <div title=" " class="link" >Create new label</div>
    </div>
   </div>

  </div>
 
<?php endif; ?>

<?php 
if(!empty($settings_available)) :?>
</div>
<span style="clear:both; display:block;"></span>
<?php 
endif
?>


    <table class="<?php print $class; ?>"<?php print $attributes; ?>>
<?php if (!empty($title)) : ?>
        <caption><?php print $title; ?></caption>
<?php endif; ?>
      <thead class="views-head">
        <tr>
<?php foreach ($header as $field => $label): ?>
      <?php //if ($field && $label){?>
        <th class="<?php print $header_classes[$field]; ?>">
<?php print $label; ?>
        </th>
<?php //} ?>
      <?php endforeach; ?>
      </tr>
    </thead>
    <tbody class="views-body">
<?php foreach ($rows as $count => $row): ?>
        <tr class="<?php print implode(' ', $row_classes[$count]); ?>">
<?php foreach ($row as $field => $content): ?>
      <?php //if ($field ) {?>
            <td class="<?php print $field_classes[$field][$count]; ?>"><?php print $content; ?></td>
<?php //} ?>
      <?php endforeach; ?>
          </tr>
<?php endforeach; ?>
  </tbody>
</table>
