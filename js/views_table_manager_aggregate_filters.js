
Drupal.behaviors.table_filter_manager = function(context) {

  if(!$('.view fieldset').hasClass('processed')){	
    $('.view-filters').wrap('<fieldset class="collapsed"/>');
    $('.view fieldset').addClass('collapsible ');
    $('.view fieldset').prepend('<legend>Search</legend>')
    $('.view fieldset').addClass('processed');
    $('.views-submit-button').append('<input type="hidden" name="table_manager_filter" value="1" >')
    Drupal.behaviors.collapse();
	
    $('.view-filters .form-text').each(function(){
      if($(this).val()){
        if($('.view fieldset').hasClass('collapsed')){
        //  Drupal.toggleFieldset($('.view fieldset'))
          return false;
        }
      }
    })
	  
    $('.view-filters select').each(function(){
      if($(this).val()){
        if($('.view fieldset').hasClass('collapsed')){
          //Drupal.toggleFieldset($('.view fieldset'))
          return false;
        }
      }
    });
	  
    $('body').ajaxComplete(function(e, xhr, settings) {
      if(settings.url.indexOf('table_manager_filter=1') > 1) {
        Drupal.settings.table_manager_filter_url = settings.url;
      }
    })
	   
    $('.view-filters #edit-reset').live('click', function() {
      $('.view-filters .form-text').val('');
      $('.view-filters select').val('');
      Drupal.settings.table_manager_filter_url = false;
    })
	    
  }
  
 

  
}