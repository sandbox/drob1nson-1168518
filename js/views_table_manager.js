
/**
 *
 */
function view_save_col_data(settings, view) {
  var cols = [];

  $('th', view).each(function() {
    cols[cols.length] = $.trim($(this).attr('class'));
  });

  $.ajax({
    url: Drupal.settings.basePath + '/viewstablemanager/save',
    type: 'POST',
    data: {
      'cols[]': cols,
      'view': settings.view_name
    },
    success: function(response) {
    },
    dataType: 'json'
  });
}