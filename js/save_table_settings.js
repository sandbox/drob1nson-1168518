Drupal.behaviors.save_table_settings = function(context) {
	
  $.each(Drupal.settings.views_table_manager, function(i, settings) {
    var view = Drupal.table_column_manager.settings_sel_view(settings, context)
    // skip processing this view if it does not exist in context
    if (!view) {
      // returning a non-false value in a $.each look is the same as doing 'continue' in a for-loop
      return true;
    }

    $('.save-settings-wrapper .label-row-wrapper').live('mouseover', function(event, ui) {
      $(this).addClass('hover');
      $(this).find('.arrow-span').show();
    });
	
    $('.save-settings-wrapper .label-row-wrapper').live('mouseout', function(event, ui) {
      $(this).removeClass('hover');
      if($(this).find('.arrow-span').attr('open') != 'true') {
        $(this).find('.arrow-span').hide();
      }
    });
	
    $('.save-settings-wrapper .save-table-settings').live('click', function(event, ui) {
    
      var type = $(this).parents('.save-settings-wrapper').attr('id');
      
      $.ajax({
        url: Drupal.settings.basePath + 'viewstablemanager/getlabels/' + settings.view_name + '/' + type,
        async: true,
        cache: false,
        success: function(html){
          $('#'+type+' .labels-wrapper .custom-labels').html(html);
          var height = $('#'+type+'.save-settings-wrapper .labels-wrapper').outerHeight(); 
          $('#'+type+' .save-settings-wrapper .labels-wrapper').css('margin-top','-'+height+'px');
        }
      });
      $('#'+type+'  .labels-wrapper').toggle();;
    });
	
    //save a label
    $('.save-settings-wrapper .link').bt({
    	
      padding: 5,
      trigger: 'click',
      positions: ['top'],
      spikeLength: 10,
      spikeGirth: 5,
      fill: 'rgba(0, 0, 0, .8)',
      strokeWidth: 1,
      strokeStyle: '#CC0',
      cssStyles: {
        color: '#FFF',
        fontWeight: 'bold'
      },
      centerPointY:0.5,
      preShow: function() {
        $('.bt-wrapper canvas').remove();
      },
      postShow: function(box) {
        $.ajax({
          url: Drupal.settings.basePath + 'viewstablemanager/createlabel/' + settings.view_name + "/" + $(this).parents('.save-settings-wrapper').attr('id'),
          async: true,
          cache: false,
          success: function(html){
            $('.bt-content', box).html(html);
            Drupal.behaviors.ajaxSubmit();
          }
        });
      }
    });
	
    $('.save-settings-wrapper .form-submit').live('click', function(event, ui) {
    	$('#edit-filter-settings').val($('.view-filters > form').serialize());
    });    
    
    //load a label
    // @TODO: this should be integrated with the manage-cols-emblem button
    
    if(!Drupal.settings.save_filter_labels_processed) {
        $('#filters .label-row').live('click', function(event, ui) {
        	 
            var label = $(this).text();
            
            $(this).append('<img class="status-active" src="'+Drupal.settings.views_table_manager_path+"/img/status-active.gif"+'"/>');
            
            $.ajax({
              url: Drupal.settings.basePath + 'viewstablemanager/getfilterdata/' + settings.view_name +"/"+$(this).attr('label'),
              async: true,
              dataType:'json',
              cache: false,
              success: function(filter_settings){
     
            	$('.view-'+settings.view_name+' form')[0].reset();
            	
            	$.each(filter_settings, function(field, value) {
            		
            		if($('input[name='+field+']',view).length) {
            			 
            			if(value)	{
            				$('input[name='+field+']',view).val(value);
            			} else {
            				$('input[name='+field+']',view).val('');
            			}
            		};
            		        	
            		if($('select[name='+field+']',view).length) {
            			if(value) {
            				$('select[name='+field+']',view).val(value);
            			} else {
            				$('select[name='+field+']',view).val('');
            			}
            		}
    	
            	});

            	$('.view-'+settings.view_name+' fieldset').removeClass('collapsed');
            	$('.save-settings-wrapper .label-row .status-active').remove();
           
              }
            });
          });  
        Drupal.settings.save_filter_labels_processed = 1;
        }
    
    
    if(!Drupal.settings.save_table_settings) {
	    $('#profile .label-row').unbind();
	    $('#profile .label-row').live('click', function(event, ui) {
	      var label = $(this).text();
	      var type = $(this).parents('.save-settings-wrapper').attr('id');
	      $(this).append('<img class="status-active" src="'+Drupal.settings.views_table_manager_path+"/img/status-active.gif"+'"/>');  
	      $.ajax({
	        url: Drupal.settings.basePath + 'viewstablemanager/getlabeldata/' + settings.view_name +"/"+$(this).attr('label') + "/" + type,
	        async: true,
	        dataType:'json',
	        cache: false,
	        success: function(label_settings){
	
	          // update the view settings with the label settings
	          $.extend(true, settings, label_settings);
	          Drupal.table_column_manager.toggle_columns(settings, view);
	          $('.save-settings-wrapper .label-row .status-active').remove();
	        }
	      });
	    });
	    Drupal.settings.save_table_settings = 1;
    }
  }); //end each
}



Drupal.behaviors.manage_label = function(context) {
  if(!Drupal.settings.views_table_manager_labels) {
	
  $.each(Drupal.settings.views_table_manager, function(i, settings) {
    var view = Drupal.table_column_manager.settings_sel_view(settings, context);
    // skip processing this view if it does not exist in context
    if (!view) {
      // returning a non-false value in a $.each look is the same as doing 'continue' in a for-loop
      return true;
    }
    
    
    
    $('.save-settings-wrapper .arrow-span').live('click', function(event, ui) {
      $('.save-settings-wrapper .arrow-span').removeAttr('open').hide();

      $('.save-settings-wrapper .label-actions').remove();
      $(this).show();
      $(this).attr('open','true');
      $(this).parent().append('<div class="label-actions"><div class="update"><a href="javascript:void(0);" class="update-settings" title="Update the settings of this filter with the existing table layout">Update settings</a></div><div class="delete"><a href="javascript:void(0);" class="delete-settings" title="Delete this label">Delete settings</a></div></div>');
    });

    $('.save-settings-wrapper .update-settings').live('click', function(event, ui) {
    	
    	var type = $(this).parents('.save-settings-wrapper').attr('id');		
    	
      if(confirm("Update saved settings for '"+$(this).parent().parent().parent().next().text()+"'?")) {	
      var label = $(this).parent().parent().parent().next().attr('label');

      $(this).append('<img class="status-active" src="'+Drupal.settings.views_table_manager_path+"/img/status-active.gif"+'"/>');  
      
      $.ajax({
        url: Drupal.settings.basePath + 'viewstablemanager/updatelabelsettings/' + settings.view_name +"/"+ type +"/"+ label,
        async: true,
        type: 'POST',
        data: $('.view-filters > form').serialize(),
        cache: false,
        success: function(json){
    	  $('.status-active').remove();
    	  $('.save-settings-wrapper .labels-wrapper').hide();;
        }
      });
      }
    });

    $('.save-settings-wrapper .delete-settings').live('click', function(event, ui) {
    	
    	var type = $(this).parents('.save-settings-wrapper').attr('id');			
    	
      if(confirm('Are you sure you want to delete this?')) {
        var label = $(this).parent().parent().parent().next().attr('label');
        link = $(this);
        $(this).append('<img class="status-active" src="'+Drupal.settings.views_table_manager_path+"/img/status-active.gif"+'"/>');  
        
        $.ajax({
          url: Drupal.settings.basePath + 'viewstablemanager/deletelabelsettings/' + settings.view_name +"/"+ type +"/" +label,
          async: true,
          cache: false,
          success: function(json){
        	link.remove();
        	$('.save-settings-wrapper .labels-wrapper').hide();;
          }
        });
        
      }
       
    });
  });
  Drupal.settings.views_table_manager_labels = 1;
 }
}



$(document).ready(function() {
  $('body').ajaxComplete(function(e, xhr, settings) {

    if(settings.url.indexOf("/createlabel") > -1 && settings.type == "post" ) {
        var close = true;

        if(xhr.responseText){
            if(xhr.responseText.indexOf('The label already exists') != -1){
            	close = false;
            }
          }      
        
        if(close == true) {
        	alert('Successfully created label');
        	$('.labels-wrapper').hide();   
        }
        
    }
  });
	  
});


Drupal.behaviors.manage_settings_close = function(context) {
  $('body').bind("click", function(event, ui) {
	if(!$(event.target).parents('.save-settings-wrapper').html()) {
	  $('.save-settings-wrapper .labels-wrapper').hide();;
	}  
  });
}
