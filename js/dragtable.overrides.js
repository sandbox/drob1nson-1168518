// overrides file for dragtable

/**
 * Store a reference to dragEnd for later use
 */
var dragtable_dragStart_ref = dragtable.dragStart;
var dragtable_dragEnd_ref = dragtable.dragEnd;

dragtable.dragStart = function(event, id) {
  if ($(event.target).is('a')) {
    // the user clicked on the link in the table header
    return false;
  }

  // we use can't use event.target here as it is not populated in IE so
  // we can use $(this).parents instead
  var view_wrapper = $(this).parents('table.draggable').parents('div.view');
  if ($('.view .view-expanded', view_wrapper).length) {
    return false;
  }
  if ($('.resize-col', view_wrapper).length) {
    return false;
  }
  dragtable_dragStart_ref(event, id);
}

/**
 * Override the original dragEnd method
 */
dragtable.dragEnd = function (event) {
  var self = Drupal.table_column_manager;
  var view_wrapper = $(event.target).parents('table.draggable').parents('div.view');

 
  // call the original dragEnd method
  dragtable_dragEnd_ref(event);

  $.each(Drupal.settings.views_table_manager, function(i, settings) {
    var view = self.settings_sel_view(settings, view_wrapper);
    if (view) {
      self.save_columns_position(settings, view);
      // returning a non-false value in a $.each look is the same as doing 'continue' in a for-loop
      return true;
    }
  });
}
