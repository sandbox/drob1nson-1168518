
$(document).ready(function() {
  $('body').ajaxComplete(function(e, xhr, settings) {
    if(settings.url.indexOf("/viewstablemanager/managecols") > -1 && settings.type == "post") {
      $('img.manage-cols-emblem').btOff();
      // extract view_name and view_display from the url
      var url_match = /\/[^\/]+\/[^\/]+\/([^\/]+)\/([^\?]+)/.exec(settings.url);
      var view_name = url_match[1];
      var view_display = url_match[2];
      var view_selector = Drupal.table_column_manager.settings_view_selector({
        'view_name': view_name,
        'view_display': view_display
      });

      var hide_cols_selector = '', show_cols_selector = '', col_visibility = [];

      
      // build the selectors for show/hide functionality
      $(view_selector + ' th').each(function () {
        // extract the field name
        // @TODO: use self.get_views_field_css_class for this.
        var match = /views-field-([a-zA-Z0-9\-_]+)/.exec($(this).attr('class'));
        if (!match || !match[1]) {
          // continue
          return true;
        }
        
        if(settings.data.indexOf(match[1]) < 1){
          hide_cols_selector += '.views-field-' + match[1] + ', ';
          col_visibility[match[1]] = 0;
        } else {
          show_cols_selector += '.views-field-' + match[1] + ', ';
          col_visibility[match[1]] = match[1];
        }
      });

      // update the view's settings
      $.each(Drupal.settings.views_table_manager, function(i, settings) {
        if (settings.view_name == view_name && settings.view_display == view_display) {
          settings.col_visibility = col_visibility;
          return false;
        }
      });

      if (hide_cols_selector) {
        $(hide_cols_selector, $(view_selector)).hide();
      }
      if (show_cols_selector) {
        $(show_cols_selector, $(view_selector)).show();
      }
    }
  });
});



Drupal.table_column_manager = Drupal.table_column_manager || {};

Drupal.table_column_manager.settings_view_selector = function(settings) {
  return '.view-id-' + settings.view_name + '.view-display-id-' + settings.view_display;
}

Drupal.table_column_manager.settings_sel_view = function(settings, context) {
  var view_selector = Drupal.table_column_manager.settings_view_selector(settings);

  // in case the context is the view wrapper then return it as it is
  var context_is_the_view = false, view_selector_classes = view_selector.split('.');
  for(var i in view_selector_classes) {
    if(!view_selector_classes[i]) {
      continue;
    }
    context_is_the_view = true;
    if (!$(context).hasClass(view_selector_classes[i])) {
      context_is_the_view = false;
      break;
    }
  }
  if (context_is_the_view) {
    return context;
  }

  // skip processing this view if it does not exist in context
  if (!$(view_selector, context).length) {
    return false;
  }

  // the view wrapper is within the context
  return $(view_selector, context);
}

Drupal.table_column_manager.get_views_field_css_class = function(string) {
  var match = /views-field-([a-zA-Z0-9\-_]+)/.exec(string);
  if (!match || !match[1]) {
    return false;
  }
  return 'views-field-' + match[1];
}

/**
 * Saves the columns position after a drag and drop action
 */
Drupal.table_column_manager.save_columns_position = function (view_settings, view_context) {
	
  var cols = [];
  $('th', view_context).each(function() {
    cols[cols.length] = $.trim($(this).attr('class'));
  });

  $.ajax({
	  
    url: Drupal.settings.basePath + 'viewstablemanager/save',
    type: 'POST',
    dataType: 'json',
    async : true,
    cache : false,
    data: {
      'cols[]': cols,
      'view': view_settings.view_name
    },
    success:function(){
    	console.log('saved')
    },  

  });
}

/**
 * Hide columns that need to be hidden
 */
Drupal.table_column_manager.toggle_columns = function (view_settings, view_context) {
  if (typeof view_settings.col_visibility == 'undefined') {
    return;
  }
 
  var hide_cols_selector = '', show_cols_selector = '', visible;

  for(var col in view_settings.col_visibility) {
    visible = view_settings.col_visibility[col];
    if (visible != 0) {
      show_cols_selector += '.views-field-' + col + ', ';
    }
    else {
      hide_cols_selector += '.views-field-' + col + ', ';
    }
  }
  
  if (hide_cols_selector) {
    $(hide_cols_selector, view_context).hide();
  }
  if (show_cols_selector) {
    $(show_cols_selector, view_context).show();
  }
}

/**
 * Add or update view settings in Drupal.settings.views_table_manager
 */
Drupal.table_column_manager.add_update_tcm_view_settings = function(new_settings, context) {
  var updated = false;
  $.each(Drupal.settings.views_table_manager, function(i, settings) {
    if (settings.view_name == new_settings.view_name && settings.view_display == new_settings.view_display) {
      settings = $.extend(true, settings, new_settings);
      updated = true;
      // get out of loop
      return false;
    }
  });
  if (!updated) {
    Drupal.settings.views_table_manager.push(new_settings);
  }
}

Drupal.table_column_manager.manage_columns = function (view_settings, view_context) {
  if (typeof view_settings.manage_columns == 'undefined') {
    return;
  }
  var self = Drupal.table_column_manager;
 
  self.toggle_columns(view_settings, view_context);

  // show manage columns button
  $('.manage-cols-emblem', view_context).bt({
    ajaxPath: Drupal.settings.basePath + 'viewstablemanager/managecols/' + view_settings.view_name + '/' + view_settings.view_display,
    ajaxOpts: {
      async: true,
      cache: false
    },
    ajaxCache: false,
    trigger: 'click',
    positions: ['left', 'right'],
    padding: 5,
    shrinkToFit: true,
    fill: '#FFF',
    cssClass: 'manage-cols-tooltip',
    centerPointY: 0,
    preShow: function() {
      Drupal.behaviors.ajaxSubmit();
    }
  });
}

/**
 * Load the excanvas library so that Beautytips library works in IE.
 */
Drupal.table_column_manager.init_ie_excanvas = function() {
  var self = Drupal.table_column_manager;
  if (!('ie_canvas_inited' in self)) {
    self.ie_canvas_inited = true;
    if ($.browser.msie) {
      $.getScript(Drupal.settings.basePath + Drupal.settings.views_table_manager_bt_path + '/other_libs/excanvas_r3/excanvas.js');
    }
  }
}

/**
 * Drupal behaviors
 */
Drupal.behaviors.table_column_manager = function(context) {
  // need to convert dragtable to support behaviors
  if (context != document) {
    $('table.draggable', context).each(function() {
      dragtable.makeDraggable(this);
    });
  }

  var self = Drupal.table_column_manager;

  // load the excanvas library if necessary
  self.init_ie_excanvas();

  $.each(Drupal.settings.views_table_manager, function(i, settings) {
    var view = Drupal.table_column_manager.settings_sel_view(settings, context);
    // skip processing this view if it does not exist in context
    if (!view) {
      // returning a non-false value in a $.each look is the same as doing 'continue' in a for-loop
      return true;
    }

    self.manage_columns(settings, view);
  });
}

