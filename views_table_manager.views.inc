<?php
/**
 * @file
 * This file contains views plugin functions
 */


/**
 * Implementation of hook_views_plugins().
 */
function views_table_manager_views_plugins() {
  return array(
    'style' => array(
      'tablemanager' => array(
        'title' => t('Table Manager'),
        'help' => t('Allows a user to swap columns around in search results'),
        'handler' => 'views_table_manager_plugin_style_table_manager',
        'theme' => 'views_view_tablemanager',
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses row plugin' => TRUE,
        'type' => 'normal',
        'parent' => 'table',
      ),
    ),
  );
}