<?php
/**
 * @file
 * This file displays options for the views plugin
 */

class views_table_manager_plugin_style_table_manager extends views_plugin_style_table {

  function option_definition() {
    $options = parent::option_definition();
    $options['drag_columns'] = array('default' => '');
    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['drag_columns'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow drag and drop columns'),
      '#default_value' => $this->options['drag_columns'],
      '#description' => t('Check this box if the columns on the table should have the ability to be drag and dropped')
    );

    $form['manage_columns'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow the user to choose which columns to display'),
      '#default_value' => $this->options['manage_columns'],
      '#description' => t('Check this box to allow users to choose the columns to display')
    );

    $form['aggregate_exposed_filters'] = array(
      '#type' => 'checkbox',
      '#title' => t('Aggregate exposed filters'),
      '#default_value' => $this->options['aggregate_exposed_filters'],
      '#description' => t('Aggregate exposed filters, i.e. collapse multiple text / date fields into a fieldset')
    );
    
    $form['save_table_settings'] = array(
      '#type' => 'checkbox',
      '#title' => t('Let users save table settings'),
      '#default_value' => $this->options['save_table_settings'],
      '#description' => t('Check this box to let users create labels to store column width, visibility & ordering settings')
    );

    $form['save_filter_settings'] = array(
      '#type' => 'checkbox',
      '#title' => t('Let users save exposed filter settings'),
      '#default_value' => $this->options['save_filter_settings'],
      '#description' => t('Check this box to let users create labels to store exposed filter settings')
    );      
  }

}