--SUMMARY--

This module provides user based filtering and management of views.

Options available are:

 - Drag and drop table columns
 - Show / hide certain columns
 - Save configurations of visible columns
 - Save configurations of filter values
 - Wrap filtered fields into a fieldset

Each saved configration e.g. column positioning / visibility is saved on a per user basis.

--REQUIREMENTS--

 - axaxsubmit
 - jquery_update
 - views

--CONFIGURATION--
 
Within a view edit screen:

1. Under 'Style Settings' -> 'Style', choose 'Table Manager' as the style
2. Click on the 'gears icon'
3. Enable the functionality required
4. Enjoy!.

--USAGE--

The icons at the top right of the view allow the following functionality:
 - Show / hide view columns
 - Save visibility settings of columns e.g. a filter can be created to display 10 columns,
   another filter can be created to display 5 columns.
 - Save 'filter' settings .i.e. data populated in exposed filters

--TROUBLESHOOTING--

Q.  I've added new fields to my view, but they don't appear.

A.  Reinstall the module, this will recreate the tables, unfortuately
    this will remove all the data too :(. This is a bug which needs
    addressing.